package music.tags;

import java.io.IOException;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class LabeledInputTag extends TagSupport {
    private static final long serialVersionUID = 1L;

    private int     size;
    private int     maxLength;
    private String  value;
    private boolean readonly  = false;
    private boolean required  = false;

    @Override
    public int doStartTag() {
        printFormat("<span%2$s><label for=\"%1$s\">", id, required ? " class=\"required\"" : "");
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doEndTag() {
        printFormat(
            "</label><input type=\"text\" name=\"%1$s\" id=\"%1$s\" size=\"%2$d\" maxLength=\"%3$d\" value=\"%4$s\"%5$s></span>",
            id,
            size,
            maxLength,
            value,
            readonly ? " readonly" : ""
        );
        return EVAL_PAGE;
    }

    private void printFormat(String format, Object... args) {
        JspWriter out = pageContext.getOut();
        String output = String.format(format, args);
        try {
            out.print(output);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
