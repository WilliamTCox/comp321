package music.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import music.models.Product;

public class ProductDB {
    private static String TABLE_NAME = "Product";

    public static List<Product> getProducts() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        Statement s = null;
        ResultSet rs = null;

        String query = "SELECT * FROM " + TABLE_NAME;

        try {
            s = connection.createStatement();
            rs = s.executeQuery(query);

            List<Product> productList = new ArrayList<Product>();
            while (rs.next()) {
                productList.add(productFromRow(rs));
            }
            return productList;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closeStatement(s);
            pool.freeConnection(connection);
        }
    }

    public static Product getProduct(String productCode) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE ProductCode = ? LIMIT 1";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, productCode);
            rs = ps.executeQuery();

            if (rs.next()) {
                return productFromRow(rs);
            }
            return null;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closeStatement(ps);
            pool.freeConnection(connection);
        }
    }

    public static int insertProduct(Product product) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query = "INSERT INTO " + TABLE_NAME
            + " (ProductCode, ProductDescription, ProductPrice) VALUES (?, ?, ?)";

        try {
            ps = connection.prepareStatement(query);

            ps.setString(1, product.getCode());
            ps.setString(2, product.getDescription());
            ps.setDouble(3, product.getPrice());

            return ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closeStatement(ps);
            pool.freeConnection(connection);
        }
    }

    public static int updateProduct(Product product) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query = "UPDATE " + TABLE_NAME + " SET ProductDescription = ?, ProductPrice = ? WHERE ProductCode = ?";

        try {
            ps = connection.prepareStatement(query);

            ps.setString(1, product.getDescription());
            ps.setDouble(2, product.getPrice());
            ps.setString(3, product.getCode());

            return ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closeStatement(ps);
            pool.freeConnection(connection);
        }
    }

    public static int deleteProduct(Product product) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query = "DELETE FROM " + TABLE_NAME + " WHERE ProductCode = ?";

        try {
            ps = connection.prepareStatement(query);

            ps.setString(1, product.getCode());

            return ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closeStatement(ps);
            pool.freeConnection(connection);
        }
    }

    private static Product productFromRow(ResultSet rs) throws SQLException {
        Product product = new Product();

        product.setCode(rs.getString("ProductCode"));
        product.setDescription(rs.getString("ProductDescription"));
        product.setPrice(rs.getDouble("ProductPrice"));

        return product;
    }
}
