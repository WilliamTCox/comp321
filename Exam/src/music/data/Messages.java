package music.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Messages implements Serializable {
    private static final long serialVersionUID = 1L;

    Map<String, String> messageMap;

    public Messages(String filePath) {
        messageMap = new HashMap<String, String>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(filePath));

            String line;
            while ((line = reader.readLine()) != null) {
                String tokens[] = line.split("\\|", 2);

                String name = tokens[0].trim();
                String message = tokens[1].trim();

                messageMap.put(name, message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            safeClose(reader);
        }
    }

    public String getMessage(String name, Object... arguments) {
        return String.format(messageMap.get(name), arguments);
    }

    private void safeClose(Reader reader) {
        try {
            if (reader != null) {
                reader.close();
            }
        } catch (IOException e) {

        }
    }
}
