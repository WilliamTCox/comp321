package music.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import music.data.Messages;
import music.data.ProductDB;

@WebServlet(
    urlPatterns = "/displayProducts",
    initParams = { @WebInitParam(name = "messagesFilePath", value = "/WEB-INF/messages.txt") }
)
public class DisplayProductsServlet extends MusicServlet {
    private static final long serialVersionUID = 1L;

    private static final String PAGE_URL = "/WEB-INF/display_products.jsp";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        String relativePath = config.getInitParameter("messagesFilePath");
        String filePath = getServletContext().getRealPath(relativePath);
        Messages messages = new Messages(filePath);
        getServletContext().setAttribute("messages", messages);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleHeader(req);

        req.setAttribute("productList", ProductDB.getProducts());

        forwardUrl(req, resp, PAGE_URL);
    }
}
