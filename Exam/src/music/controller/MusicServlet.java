package music.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import music.data.Messages;

public abstract class MusicServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String DISPLAY_PRODUCTS_REDIRECT = "displayProducts";

    protected void handleHeader(HttpServletRequest req) {
        req.setAttribute("message", retrieveSessionMessage(req));
    }

    protected String getMessage(String name, Object... arguments) {
        Messages messages = (Messages)getServletContext().getAttribute("messages");
        return messages.getMessage(name, arguments);
    }

    protected void setSessionMessage(HttpServletRequest req, String message) {
        req.getSession().setAttribute("message", message);
    }

    protected Object retrieveSessionAttribute(HttpServletRequest req, String name) {
        HttpSession session = req.getSession();
        final Object sessionLock = session.getId().intern();

        synchronized (sessionLock) {
            Object value = session.getAttribute(name);
            session.removeAttribute(name);
            return value;
        }
    }

    protected String retrieveSessionMessage(HttpServletRequest req) {
        return (String)retrieveSessionAttribute(req, "message");
    }

    protected void forwardUrl(HttpServletRequest req, HttpServletResponse resp, String url)
        throws ServletException, IOException
    {
        getServletContext().getRequestDispatcher(url).forward(req, resp);
    }

    protected void forwardUrl(HttpServletRequest req, HttpServletResponse resp, String url, String message)
        throws ServletException, IOException
    {
        req.setAttribute("message", message);
        forwardUrl(req, resp, url);
    }

    protected void redirectToDisplayProducts(HttpServletResponse resp) throws IOException {
        resp.sendRedirect(DISPLAY_PRODUCTS_REDIRECT);
    }

    protected void redirectToDisplayProducts(HttpServletRequest req, HttpServletResponse resp, String message)
        throws IOException
    {
        setSessionMessage(req, message);
        redirectToDisplayProducts(resp);
    }
}
