package music.controller;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import music.models.Product;

public abstract class UpdateProductServlet extends MusicServlet {
    private static final long serialVersionUID = 1L;

    private static final String PAGE_URL = "/WEB-INF/update_product.jsp";

    private int productCodeMaxLength;
    private int productDescriptionMaxLength;
    private int productPriceMaxLength;
    private double productPriceMaxValue;

    @Override
    public void init() {
        ServletContext context = getServletContext();

        productPriceMaxValue = Double.parseDouble(context.getInitParameter("productPriceMaxValue"));
        
        productCodeMaxLength = Integer.parseInt(context.getInitParameter("productCodeMaxLength"));
        productDescriptionMaxLength = Integer.parseInt(context.getInitParameter("productDescriptionMaxLength"));
        productPriceMaxLength = String.format("%.2f", productPriceMaxValue).length();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleHeader(req);

        String code = req.getParameter("productCode");
        String description = req.getParameter("productDescription");
        String price = req.getParameter("productPrice");

        HttpSession session = req.getSession();
        final Object sessionLock = session.getId().intern();

        synchronized (sessionLock) {
            try {
                Product product = createProduct(code, description, price);
                String message = performRequestAction(req, product);

                removeSessionAttributes(req);
                redirectToDisplayProducts(req, resp, message);
            } catch (IllegalStateException e) {
                removeSessionAttributes(req);
                redirectToDisplayProducts(req, resp, e.getMessage());
            } catch (IllegalArgumentException e) {
                setRequestAttributes(req, code, description, price);
                forwardUrl(req, resp, PAGE_URL, e.getMessage());
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleHeader(req);
        forwardUrl(req, resp, PAGE_URL);
    }

    protected void setRequestAttributes(
        HttpServletRequest req,
        String productCode,
        String productDescription,
        String productPrice
    )
    {
        req.setAttribute("updateProductAction", req.getContextPath() + req.getServletPath());
        req.setAttribute("productCode", truncateString(productCode, productCodeMaxLength));
        req.setAttribute("productDescription", truncateString(productDescription, productDescriptionMaxLength));
        req.setAttribute("productPrice", productPrice);

        req.setAttribute("productCodeMaxLength", productCodeMaxLength);
        req.setAttribute("productDescriptionMaxLength", productDescriptionMaxLength);
        req.setAttribute("productPriceMaxLength", productPriceMaxLength);

        req.setAttribute("productCodeRequired", true);
        req.setAttribute("productCodeReadonly", false);
    }

    protected void removeSessionAttributes(HttpServletRequest req) {

    }

    protected abstract String performRequestAction(HttpServletRequest req, Product product);

    private Product createProduct(String code, String description, String price) {
        if (code == null || code.isEmpty()) {
            throw new IllegalArgumentException(getMessage("MISSING_PRODUCT_CODE"));
        } else if (code.length() > productCodeMaxLength) {
            throw new IllegalArgumentException(getMessage("PRODUCT_CODE_TOO_LONG", productCodeMaxLength));
        }

        if (description == null || description.isEmpty()) {
            throw new IllegalArgumentException(getMessage("MISSING_PRODUCT_DESCRIPTION"));
        } else if (description.length() > productDescriptionMaxLength) {
            throw new IllegalArgumentException(getMessage("PRODUCT_DESCRIPTION_TOO_LONG", productDescriptionMaxLength));
        }

        if (price == null || price.isEmpty()) {
            throw new IllegalArgumentException(getMessage("MISSING_PRODUCT_PRICE"));
        }

        BigDecimal parsedPrice = null;
        try {
            parsedPrice = new BigDecimal(price);
        } catch (Exception e) {
            parsedPrice = new BigDecimal(-1);
        }
        double priceValue = parsedPrice.doubleValue();
        boolean validPrice = parsedPrice.scale() <= 2 && priceValue >= 0;

        if (!validPrice) {
            throw new IllegalArgumentException(getMessage("INVALID_PRODUCT_PRICE", price));
        }
        if (priceValue >= productPriceMaxValue) {
            throw new IllegalArgumentException(getMessage("PRODUCT_PRICE_TOO_HIGH", price, productPriceMaxValue));
        }

        return new Product(code, description, priceValue);
    }

    private static String truncateString(String str, int length) {
        if (str == null || str.length() < length) {
            return str;
        }
        return str.substring(0, length);
    }
}
