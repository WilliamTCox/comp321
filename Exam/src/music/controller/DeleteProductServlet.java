package music.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import music.data.ProductDB;
import music.models.Product;

@WebServlet("/deleteProduct")
public class DeleteProductServlet extends MusicServlet {
    private static final long serialVersionUID = 1L;

    private static final String PAGE_URL = "/WEB-INF/delete_product.jsp";

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleHeader(req);

        HttpSession session = req.getSession();
        final Object sessionLock = session.getId().intern();

        String productCode = req.getParameter("productCode");

        String message;
        synchronized (sessionLock) {
            Product product = (Product)retrieveSessionAttribute(req, "deleteProduct");
            if (product == null || productCode == null || !product.getCode().equals(productCode)) {
                message = getMessage("ACTION_CANCELLED");
            } else {
                ProductDB.deleteProduct(product);
                message = getMessage("PRODUCT_DELETED", product.getCode());
            }
        }

        redirectToDisplayProducts(req, resp, message);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleHeader(req);

        String productCode = req.getParameter("productCode");
        Product product = ProductDB.getProduct(productCode);

        req.getSession().setAttribute("deleteProduct", product);
        req.setAttribute("product", product);

        forwardUrl(req, resp, PAGE_URL);
    }
}
