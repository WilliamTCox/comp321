package music.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import music.data.ProductDB;
import music.models.Product;

@WebServlet("/addProduct")
public class AddProductServlet extends UpdateProductServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setRequestAttributes(req, null, null, null);
        super.doGet(req, resp);
    }

    @Override
    protected String performRequestAction(HttpServletRequest req, Product product) {
        String code = product.getCode();

        synchronized (ProductDB.class) {
            if (ProductDB.getProduct(code) != null) {
                throw new IllegalArgumentException(getMessage("PRODUCT_CODE_EXISTS", code));
            }

            ProductDB.insertProduct(product);
        }

        return getMessage("PRODUCT_ADDED", code);
    }
}
