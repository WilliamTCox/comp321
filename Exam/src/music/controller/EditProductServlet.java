package music.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import music.data.ProductDB;
import music.models.Product;

@WebServlet("/editProduct")
public class EditProductServlet extends UpdateProductServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String productCode = req.getParameter("productCode");
        req.getSession().setAttribute("editProductCode", productCode);

        Product product = ProductDB.getProduct(productCode);
        setRequestAttributes(req, product.getCode(), product.getDescription(), Double.toString(product.getPrice()));

        super.doGet(req, resp);
    }

    @Override
    protected void setRequestAttributes(
        HttpServletRequest req,
        String productCode,
        String productDescription,
        String productPrice
    )
    {
        super.setRequestAttributes(req, productCode, productDescription, productPrice);
        req.setAttribute("productCodeRequired", false);
        req.setAttribute("productCodeReadonly", true);
    }

    @Override
    protected String performRequestAction(HttpServletRequest req, Product product) {
        String sessionCode = (String)req.getSession().getAttribute("editProductCode");
        String productCode = product.getCode();

        if (sessionCode == null || !productCode.equals(sessionCode)) {
            throw new IllegalStateException(getMessage("ACTION_CANCELLED"));
        }

        synchronized (ProductDB.class) {
            if (ProductDB.getProduct(productCode) == null) {
                throw new IllegalStateException(getMessage("ACTION_CANCELLED"));
            }

            ProductDB.updateProduct(product);
        }

        return getMessage("PRODUCT_EDITED", productCode);
    }

    @Override
    protected void removeSessionAttributes(HttpServletRequest req) {
        req.getSession().removeAttribute("editProductCode");
    }
}
