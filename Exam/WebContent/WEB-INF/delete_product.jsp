<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:import url="/WEB-INF/header.jsp">
    <c:param name="heading" value="Are you sure you want to delete this product?" />
</c:import>

<span class="label">Code:</span>
<span id="productCode">${fn:escapeXml(product.code)}</span>
<br>

<span class="label">Description:</span>
<span id="productDescription">${fn:escapeXml(product.description)}</span>
<br>

<span class="label">Price:</span>
<span id="productPrice">${fn:escapeXml(product.priceCurrencyFormat)}</span>
<br>

<form action="deleteProduct" method="post">
    <input type="hidden" name="productCode" value="${fn:escapeXml(product.code)}">
    <input type="submit" value="Yes">
</form>
<form action="displayProducts" method="get">
    <input type="submit" value="No">
</form>

<c:import url="/WEB-INF/footer.jsp" />
