<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:import url="/WEB-INF/header.jsp">
    <c:param name="heading" value="Products" />
</c:import>

<table>
    <tr>
        <th>Code</th>
        <th>Description</th>
        <th>Price</th>
        <th></th>
        <th></th>
    </tr>
    <c:forEach var="product" items="${productList}">
        <tr>
            <td>${fn:escapeXml(product.code)}</td>
            <td>${fn:escapeXml(product.description)}</td>
            <td>${fn:escapeXml(product.priceCurrencyFormat)}</td>
            <td>
                <a href="editProduct?productCode=${fn:escapeXml(product.code)}">Edit</a>
            </td>
            <td>
                <a href="deleteProduct?productCode=${fn:escapeXml(product.code)}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="addProduct" method="get">
    <input type="submit" value="Add Product">
</form>

<c:import url="/WEB-INF/footer.jsp" />
