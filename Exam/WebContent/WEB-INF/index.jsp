<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="/WEB-INF/header.jsp">
    <c:param name="heading" value="Product Maintenance" />
</c:import>

<a href="./displayProducts">View Products</a>

<c:import url="/WEB-INF/footer.jsp" />
