<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="music" uri="/WEB-INF/music.tld"%>

<c:import url="/WEB-INF/header.jsp">
    <c:param name="heading" value="Product" />
</c:import>

<p>
    <span class="required"></span>
    marks required fields
</p>
<form action="${updateProductAction}" method="post">
    <music:labeledInput id="productCode" value="${fn:escapeXml(productCode)}" size="8"
        maxLength="${productCodeMaxLength}" required="${productCodeRequired}" readonly="${productCodeReadonly}">Code:</music:labeledInput>
    <br>

    <music:labeledInput id="productDescription" value="${fn:escapeXml(productDescription)}" size="64"
        maxLength="${productDescriptionMaxLength}" required="true">Description:</music:labeledInput>
    <br>

    <music:labeledInput id="productPrice" value="${fn:escapeXml(productPrice)}" size="8"
        maxLength="${productPriceMaxLength}" required="true">Price:</music:labeledInput>
    <br>

    <span class="label">&nbsp;</span>
    <input type="submit" value="Update Product">
</form>
<form action="displayProducts" method="get">
    <input type="submit" value="View Products">
</form>

<c:import url="/WEB-INF/footer.jsp" />
